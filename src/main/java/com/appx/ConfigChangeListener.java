package com.appx;

public interface ConfigChangeListener {

  public void propertyChanged(String key, Object value);

}
